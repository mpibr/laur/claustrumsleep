%% ex vivo
cd('/storage/laur/data_3/HiroakiNorimoto/Ephys/ex vivo claustrum/ex vivo')

datasets={'18131_003.atf','181019_004.atf','180509004.atf'}
fs=2000;

for currDataset=1:3
    [~,~,~,d]=import_atf(datasets{currDataset});
    v= downsample(d(:,2),2)*1000; %ms and uv
    
    [bb,aa]=butter(2,[20/(1000/2)],'low');
    vFilt=filtfilt(bb,aa,v);
    
    %detect sharp waves like I do for filter construction invivo
    [peaks,peakTime,widths,peakProminance]=findpeaks(-vFilt,...
        'MinPeakHeight',3*std(vFilt),'MinPeakDistance',200,'MinPeakProminence',0.5*std(vFilt)/2, 'MinPeakWidth',5,'WidthReference','halfheight');
    
    [ev_avg,lags,ev_mat] = getEventTrigAvg(vFilt,peakTime,1000,1500);
    
    
    %autocorrelation of SWs
    % filter and downsample data
    swBpp=zeros(1,length( v));
    swBpp(round(peakTime))=1000;
    [bb1,aa1]=butter(2,[0.05/(1000/2)],'low')
    filtSW=filtfilt(bb1,aa1,swBpp);
    filtSWDs=downsample(filtSW,100);
    
    %autocorrelation of the SW rate
    [acf,lags,bounds] = autocorr(filtSWDs,'NumLags',5000);
    
    
    
    meanWidth=mean(widths);
    meanAmp=mean(peaks);
    meanISI=mean(diff(peakTime));
    save([datasets{currDataset} '_analysis.mat'],'ev_avg','meanWidth','meanISI','meanAmp', 'acf', 'lags');
end

%% for the example dataset, choose 1

currDataset=1;
[~,~,~,d]=import_atf(datasets{currDataset});
v= downsample(d(:,2),2)*1000; %ms and uv


[bb,aa]=butter(2,[20/(1000/2)],'low');
vFilt=filtfilt(bb,aa,v);

%detect sharp waves like I do for filter construction invivo
[peaks,peakTime,widths,peakProminance]=findpeaks(-vFilt,...
    'MinPeakHeight',3*std(vFilt),'MinPeakDistance',200,'MinPeakProminence',0.5*std(vFilt)/2, 'MinPeakWidth',5,'WidthReference','halfheight');

[ev_avg,lags,ev_mat] = getEventTrigAvg(vFilt,peakTime,1000,1500);

meanWidth=mean(widths);
meanAmp=mean(peaks);
meanISI=mean(diff(peakTime));

figure
scatter(peaks,widths,5,'filled')
title('SW peaks and widths exvivo')
xlabel('amp (uV)')
ylabel('width (fwhm)')

figure
histogram(peaks,'Normalization','probability')
title('SW amplitude histogram exvivo')
xlabel('amp (uV)')
ylabel('probability')

figure
histogram(widths,'Normalization','probability')
title('SW width histogram exvivo')
xlabel('width (fwhm)')
ylabel('probability')

figure
plot(lags, ev_avg,'k','linewidth',2)
title('avg exvivo SW')
ylabel('amp (uV)')
xlabel('time')
xlim([-1000 1500])

figure
hold on

eb=std(ev_mat)';
% for x=1:100
%     plot(lags,ev_mat(x,:),'r','linewidth',0.01)
% end
plot(lags, ev_avg,'k','linewidth',2)
plot(lags, ev_avg+eb,'r')
plot(lags, ev_avg-eb,'r')
title('avg exvivo SW +/- 1 std')
ylabel('amp (uV)')
xlabel('time')
xlim([-1000 1500])


figure
histogram(diff(peakTime)/1000,'Normalization','probability')
title('SW inter-event interval distribution during sleep')
xlabel('time (s)')
ylabel('probability')

%% pop analysis exvivo

cd('/storage/laur/data_3/HiroakiNorimoto/Ephys/ex vivo claustrum/ex vivo')

datasets={'18131_003.atf','181019_004.atf','180509004.atf'}
amp=[]; ISI=[]; width=[];avgW=[];
for currDataset=1:3
    load([datasets{currDataset} '_analysis.mat']);
    amp(currDataset)=meanAmp;
    ISI(currDataset)=meanISI;
    width(currDataset)=meanWidth;
    avgW(currDataset,:)=ev_avg;
end


figure
hold on
for x=1:size(avgW,1)
    plot(avgW(x,:))
end




%% slice
cd('/storage/laur/data_3/PT slice SWR/whole slice')

datasets={'180524006_reduced_I9-PT.atf','180529001_reduced_H13-PT 0-600s.atf',...
    '180605001_reduced_N15-PT3 0-600s.atf','180626001_reduced_E12-PT 0-600s.atf',...
    '180626004_reduced_E12-PT 0-600s.atf','180920001_reduced_H7-PT.atf','181001002_reduced_D8-PT.atf',...
    '181008001_reduced_H9-PT.atf','181008002_reduced_M7-PT.atf'}
fs=2000;

for currDataset=1:numel(datasets)
    [~,~,~,d]=import_atf(datasets{currDataset});
    v= downsample(d(:,2),2); %ms and uv
    
    [bb,aa]=butter(2,[20/(1000/2)],'low');
    vFilt=filtfilt(bb,aa,v);
    
    %detect sharp waves like I do for filter construction invivo
    [peaks,peakTime,widths,peakProminance]=findpeaks(-vFilt,...
        'MinPeakHeight',3*std(vFilt),'MinPeakDistance',200,'MinPeakProminence',0.5*std(vFilt)/2, 'MinPeakWidth',5,'WidthReference','halfheight');
    
    [ev_avg,lags,ev_mat] = getEventTrigAvg(vFilt,peakTime,1000,1500);
    
    
    meanWidth=mean(widths);
    meanAmp=mean(peaks);
    meanISI=mean(diff(peakTime));
    save([datasets{currDataset} '_analysis.mat'],'ev_avg','meanWidth','meanISI','meanAmp');
end

%% long recordings slice
cd('/storage/laur/data_3/PT slice SWR/whole slice/longer than 10min')

datasets={'17830000_29min.atf', '180529001_reduced_H13-PT 0-600s.atf',...
    '180605001_reduced_N15-PT3 0-600s.atf', '180626004_reduced_E12-PT 0-600s.atf',...
    '180626001_reduced_E12-PT 0-600s.atf'}
fs=2000;

for currDataset=1:numel(datasets)
    [~,~,~,d]=import_atf(datasets{currDataset});
    v= downsample(d(:,2),2); %ms and uv
    
    [bb,aa]=butter(2,[20/(1000/2)],'low');
    vFilt=filtfilt(bb,aa,v);
    
    %detect sharp waves like I do for filter construction invivo
    [peaks,peakTime,widths,peakProminance]=findpeaks(-vFilt,...
        'MinPeakHeight',3*std(vFilt),'MinPeakDistance',200,'MinPeakProminence',0.5*std(vFilt)/2, 'MinPeakWidth',5,'WidthReference','halfheight');
    
    [ev_avg,lags,ev_mat] = getEventTrigAvg(vFilt,peakTime,1000,1500);
    
    %autocorrelation of SWs
    % filter and downsample data
    swBpp=zeros(1,length( v));
    swBpp(round(peakTime))=1000;
    [bb1,aa1]=butter(2,[0.05/(1000/2)],'low')
    filtSW=filtfilt(bb1,aa1,swBpp);
    filtSWDs=downsample(filtSW,100);
    
    %autocorrelation of the SW rate
    [acf,lags,bounds] = autocorr(filtSWDs,'NumLags',5000);
    
    meanWidth=mean(widths);
    meanAmp=mean(peaks);
    meanISI=mean(diff(peakTime));
    save([datasets{currDataset} '_analysis.mat'],'ev_avg','meanWidth','meanISI','meanAmp');
end

%% for the example dataset, choose 1

currDataset=1;
[~,~,~,d]=import_atf(datasets{currDataset});
v= downsample(d(:,2),2); %ms and uv


[bb,aa]=butter(2,[20/(1000/2)],'low');
vFilt=filtfilt(bb,aa,v);

%detect sharp waves like I do for filter construction invivo
[peaks,peakTime,widths,peakProminance]=findpeaks(-vFilt,...
    'MinPeakHeight',3*std(vFilt),'MinPeakDistance',200,'MinPeakProminence',0.5*std(vFilt)/2, 'MinPeakWidth',5,'WidthReference','halfheight');

[ev_avg,lags,ev_mat] = getEventTrigAvg(vFilt,peakTime,1000,1500);

meanWidth=mean(widths);
meanAmp=mean(peaks);
meanISI=mean(diff(peakTime));

figure
scatter(peaks,widths,5,'filled')
title('SW peaks and widths exvivo')
xlabel('amp (uV)')
ylabel('width (fwhm)')

figure
histogram(peaks,'Normalization','probability')
title('SW amplitude histogram exvivo')
xlabel('amp (uV)')
ylabel('probability')

figure
histogram(widths,'Normalization','probability')
title('SW width histogram exvivo')
xlabel('width (fwhm)')
ylabel('probability')

figure
plot(lags, ev_avg,'k','linewidth',2)
title('avg slice SW')
ylabel('amp (uV)')
xlabel('time')
xlim([-1000 1500])

figure
hold on

eb=std(ev_mat)';
% for x=1:100
%     plot(lags,ev_mat(x,:),'r','linewidth',0.01)
% end
plot(lags, ev_avg,'k','linewidth',2)
plot(lags, ev_avg+eb,'r')
plot(lags, ev_avg-eb,'r')
title('avg slice SW +/- 1 std')
ylabel('amp (uV)')
xlabel('time')
xlim([-1000 1500])


figure
histogram(diff(peakTime)/1000,'Normalization','probability')
title('SW inter-event interval distribution during sleep')
xlabel('time (s)')
ylabel('probability')

%% pop analysis slice

cd('/storage/laur/data_3/PT slice SWR/whole slice')

datasets={'180524006_reduced_I9-PT.atf','180529001_reduced_H13-PT 0-600s.atf',...
    '180605001_reduced_N15-PT3 0-600s.atf','180626001_reduced_E12-PT 0-600s.atf',...
    '180626004_reduced_E12-PT 0-600s.atf','180920001_reduced_H7-PT.atf','181001002_reduced_D8-PT.atf',...
    '181008001_reduced_H9-PT.atf','181008002_reduced_M7-PT.atf'}

amp=[]; ISI=[]; width=[];avgW=[];
for currDataset=1:numel(datasets)
    load([datasets{currDataset} '_analysis.mat']);
    amp(currDataset)=meanAmp;
    ISI(currDataset)=meanISI;
    width(currDataset)=meanWidth;
    avgW(currDataset,:)=ev_avg;
end


figure
hold on
for x=1:size(avgW,1)
    plot(avgW(x,:))
end

%% comparison of rythmicity invivo and exvivo

for dataset=1:5
    %load SWs
    load([invivoDatasets{dataset} '_analysis/sharpWaves_ch32.mat']);
    load([invivoDatasets{dataset} '_analysis/slowCycles_ch32.mat']);
    load([invivoDatasets{dataset} '_analysis/dbRatio_ch32.mat']);
    load([invivoDatasets{dataset} '_analysis/sharpWavesAnalysis_ch32.mat']);
    
    %load up SWs restrict analysis to during sleep
    sleepInterp=interp1(t_ms, double(pSleepDBRatio'),1:t_ms(end));
    tSWtmp=tSW;
    swBpp=zeros(1,length(sleepInterp));
    swBpp(tSWtmp)=1000;
    swBpp(sleepInterp==0)=[];
    
    % filter and downsample data
    [bb1,aa1]=butter(2,[0.05/(1000/2)],'low')
    filtSW=filtfilt(bb1,aa1,swBpp);
    filtSWDs=downsample(filtSW,100);
    
    %autocorrelation of the SW rate
    [acf,lags,bounds] = autocorr(filtSWDs,'NumLags',5000);
    
    save([invivoDatasets{dataset} '_analysis/swAcorr_ch32.mat'],'acf','lags');
    
    dataset
end



%%
figure
hold on

allACF=zeros(5,5001);
for dataset=1:5
    load([invivoDatasets{dataset} '_analysis/swAcorr_ch32.mat']);
    allACF(dataset,:)=acf;
    
end
meanACF=mean(allACF);
plot(lags/10,meanACF)


cd('/storage/laur/data_3/HiroakiNorimoto/Ephys/ex vivo claustrum/ex vivo')
datasets={'18131_003.atf','181019_004.atf','180509004.atf'}

allACF=zeros(3,5001);
for currDataset=1:3
    load([datasets{currDataset} '_analysis.mat']);
    allACF(currDataset,:)=acf;
end
meanACF=mean(allACF);
plot(lags/10,meanACF)


cd('/storage/laur/data_3/PT slice SWR/whole slice/longer than 10min')

datasets={'17830000_29min.atf', '180529001_reduced_H13-PT 0-600s.atf',...
    '180605001_reduced_N15-PT3 0-600s.atf', '180626004_reduced_E12-PT 0-600s.atf',...
    '180626001_reduced_E12-PT 0-600s.atf'}

allACF=zeros(5,5001);
for currDataset=1:5
    load([datasets{currDataset} '_analysis.mat']);
    allACF(currDataset,:)=acf;
end
meanACF=mean(allACF);
plot(lags/10,meanACF)

xlabel('time (s)')
ylabel('autocorrelation')
legend('invivo','exvivo','slice')


%% lorenz Voltage clamp

cd('/storage.laur.corp.brain.mpg.de\Data\Lorenz Fenk\SAM');
%cd('/storage/laur/data/Lorenz Fenk/SAM')
load(['20171017aa.mat'])
lfp=data.series(3).traces(1).values;
lfp=reshape(lfp',size(lfp,1)*size(lfp,2),1);
lfp=downsample(lfp,10); % to ms
[bb,aa]=butter(2,[20/(1000/2)],'low');
vFilt=filtfilt(bb,aa,lfp);

%detect sharp waves like I do for filter construction invivo
[peaks,peakTime1,widths,peakProminance]=findpeaks(-zscore(vFilt),...
    'MinPeakHeight',3,'MinPeakDistance',200,'MinPeakProminence',0.25, 'MinPeakWidth',5,'WidthReference','halfheight');
[ev_avgLFP1,lags,ev_mat] = getEventTrigAvg(vFilt,peakTime1,1000,1500);

lfp=data.series(4).traces(1).values;
lfp=reshape(lfp',size(lfp,1)*size(lfp,2),1);
lfp=downsample(lfp,10); % to ms
[bb,aa]=butter(2,[20/(1000/2)],'low');
vFilt=filtfilt(bb,aa,lfp);

%detect sharp waves like I do for filter construction invivo
[peaks,peakTime2,widths,peakProminance]=findpeaks(-zscore(vFilt),...
    'MinPeakHeight',3,'MinPeakDistance',200,'MinPeakProminence',0.25, 'MinPeakWidth',5,'WidthReference','halfheight');
[ev_avgLFP2,lags,ev_mat] = getEventTrigAvg(vFilt,peakTime2,1000,1500);


vc1=data.series(3).traces(3).values;
vc1=reshape(vc1',size(vc1,1)*size(vc1,2),1);
vc1=downsample(vc1,10);
[ev_avgIntra1,lags,ev_mat] = getEventTrigAvg(vc1,peakTime1,1000,1500);

vc2=data.series(4).traces(3).values;
vc2=reshape(vc2',size(vc2,1)*size(vc2,2),1);
vc2=downsample(vc2,10);
[ev_avgIntra2,lags,ev_mat] = getEventTrigAvg(vc2,peakTime2,1000,1500);


figure
hold on
plot(lags, ev_avgLFP1-mean(ev_avgLFP1))
plot(lags,ev_avgLFP2-mean(ev_avgLFP2))
plot(lags, -ev_avgIntra1+mean(ev_avgIntra1))
plot(lags,ev_avgIntra2-mean(ev_avgIntra2))
legend('sws1','sws2','EPSC mag','IPSC mag','Location','SouthEast')
axis tight


