# ClaustrumSleep

analysis source code for the publication "A claustrum in reptiles and its role in slow-wave sleep" by Hiroaki Norimoto*, Lorenz A. Fenk*, Hsing-Hsi Li, Maria Antonietta Tosches, Tatiana Gallego Flores, David Hain, Sam Reiter, Riho Kobayashi, Angeles Macias, Anja Arends, Michaela Klinkmann & Gilles Laurent, Nature (2020)