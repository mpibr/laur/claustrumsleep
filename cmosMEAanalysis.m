
% Load directly from .BRW files

cd('/storage/laur/data_3/HiroakiNorimoto/CMOS/190507hor')
clear all
filename='Phase_02huge.brw';

info=h5info(filename);
chunkSize=info.Groups(1).Datasets.ChunkSize*200;
dataLength=info.Groups(1).Datasets.Dataspace.Size;


fs=h5read([filename],'/3BRecInfo/3BRecVars/SamplingRate');
minV=h5read([filename],'/3BRecInfo/3BRecVars/MinVolt');
maxV=h5read([filename],'/3BRecInfo/3BRecVars/MaxVolt');
bitD=int16(h5read([filename],'/3BRecInfo/3BRecVars/BitDepth'));
qLevel= double(2^bitD);  
fromQLevelToUVolt = (maxV - minV) / qLevel;
inversion=h5read([filename],'/3BRecInfo/3BRecVars/SignalInversion');

offset=2.0602e+03;

fsDS=500;
dsValue=round(fs/fsDS);
fsNew=fs/fsDS;
numChunks=floor(dataLength/chunkSize);
chunkList=1:chunkSize:dataLength;

lookBack=200;
lookForward=200;

allEvents=zeros(4096,lookBack+lookForward+1);

for chunk=1:numChunks
    
    %load data, downsample it
    currV=int16(h5read([filename],'/3BData/Raw',chunkList(chunk),chunkSize));
    tempData=reshape( currV,4096,length(currV)/4096);
    vMat=(tempData-(qLevel/2))*fromQLevelToUVolt;
    vVec=double(vMat(:,1:dsValue:size(vMat,2)));
    
   %flag bad channels
    bad=zeros(size(vVec,1),1);
    maxVal=max(vVec');
    bad(maxVal>500)=1;
    bad(maxVal<-500)=1;
    
    vClean=zscore(vVec')'; %normalize 
    
    [bb,aa]=butter(3,[20/(fsDS/2)],'low');
    vFilt=zeros(size(vClean));
    % vFiltNoZ=zeros(size(vClean));
    for x = 1:size(vClean,1)
        vFilt(x,:)=filtfilt(bb,aa,vClean(x,:));
       % vFiltNoZ(x,:)=filtfilt(bb,aa,vVec(x,:));
        x
    end
    
    vFilt(bad==1,:)=0;
    maxSignal=medfilt1(max(vFilt,[],2),5);
    isInWaves=maxSignal>7;
   [~,bestChannel]=max(maxSignal)
    
    if sum(isInWaves)>10
        
     [pks,locs]=findpeaks(-vFilt(bestChannel,:),'MinPeakHeight',5,'MinPeakDistance',1000);
    
     evAvg=zeros(size(vFilt,1),401);
     for ch=1:size(vClean,1)
         [evAvg(ch,:)] = getEventTrigAvg(vFilt(ch,:),locs,lookBack,lookForward);
         ch
     end
    
    % evStruct{chunk}=evAvg;
     numLocs(chunk)=numel(locs);
    
     allEvents=allEvents+evAvg*numel(locs);
    end
     
    disp(['chunk = ' num2str(chunk)])
end


 evAvg=allEvents/sum(numLocs);
 threshCross=zeros(4096,1);

for x=1:size(evAvg,1)
    clearsThresh=find(evAvg(x,:)<-1,1);
    if numel(clearsThresh)==1
        threshCross(x)=clearsThresh;
    end
    
end

map=reshape(threshCross,64,64);
filtMap=medfilt2(map,[3,3]);
filtMap(filtMap==0)=max(filtMap(:));
upsampleMap=imresize(filtMap,10)-min(filtMap(:));

figure
imagesc(-upsampleMap)
caxis([-40 -2 ])  %remember this is at 500Hz
colormap jet


%% velocity
load('evAvgMatrix.mat')
%need the distances to the seed channel
pitch=81; %microns

 threshCross=zeros(4096,1);

for x=1:size(evAvg,1)
    clearsThresh=find(evAvg(x,:)<-1,1);
    if numel(clearsThresh)==1
        threshCross(x)=clearsThresh;
    end
    
end
threshCross(threshCross<150)=300;
map=reshape(threshCross,64,64);
filtMap=medfilt2(map,[3,3]);
filtMap(filtMap==0)=max(filtMap(:));


[~,seedElectrode]=min(filtMap(:));
[I,J] = ind2sub([64 64],seedElectrode);
filtMap=(filtMap-filtMap(I,J))*2;  %delay from seed in ms


%distance to seed electrode
[colum,row]=meshgrid(1:64,1:64);
colum=reshape(colum,size(colum,1)*size(colum,2),1);
row=reshape(row,size(row,1)*size(row,2),1);
ePos=[row colum];
seedPos=repmat([I,J],4096,1);
dist2Seed=pdist2(seedPos,ePos);
dist2Seed=dist2Seed(1,:)*pitch;

%average Velocity
filtMapVec=reshape(filtMap,size(row,1)*size(row,2),1);
isZero=filtMapVec==0;
%remove seed electrode
filtMapVec(isZero)=[];
dist2Seed(isZero)=[];

noMeasurement=filtMapVec==max(filtMapVec);
filtMapVec(noMeasurement)=[];
dist2Seed(noMeasurement)=[];

avgVel=median(dist2Seed'./filtMapVec) %mm/sec 


%% the movie

figure
vTmp=reshape(evAvg,64,64,size(evAvg,2));
colormap jet
for x=1:size(evAvg,2)
    imagesc(medfilt2(squeeze(vTmp(:,:,x)),[3,3]))
    caxis ([-10 10]) %([0 60])
    pause(0.05)
end

save('evAvgMatrix.mat','evAvg')

%% put the latency map on the slice
sliceImg=imread('Phase_02huge.JPG');
figure
imagesc(sliceImg)
axis image
[xC,yC] = ginput(3); %triangle of points


tform = estimateGeometricTransform([1 64; 1 1;64 1],[xC,yC],'affine');
outputView = imref2d(size(sliceImg));
warpedImg=imwarp(filtMap,tform,'OutputView',outputView,'Interp','nearest');

figure
imagesc(warpedImg)
colormap jet
caxis([175 220])
hold on
alpha=warpedImg<=3;
i=image(sliceImg)
set(i,'AlphaData',alpha);
axis image



