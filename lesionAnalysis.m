%beta auto and cross correlations

%clear all; close all; clc

sleepStart=3*60*60; %start analysis 3 hours in. Animal is sleeping then.
sleepEnd=9*60*60; %end analysis 9 hours in. Animal is still asleep.

load('dbRatio_ch31.mat')
b1=bufferedOnlyBetaRatio;
load('dbRatio_ch62.mat')
b2=bufferedOnlyBetaRatio;

b1=b1(sleepStart:sleepEnd);
b2=b2(sleepStart:sleepEnd);

[acf1,lags,bounds] = autocorr(b1,1000);
[acf2,lags,bounds] = autocorr(b2,1000);
[xcf1,lagsX,bounds] = crosscorr(b1,b2,1000);


figure
hold on
plot(lags,acf1)
plot(lags,acf2)
xlabel('time (s)')
ylabel('correlation')
legend('control','lesion')
title('beta power autocorrelations')


figure
hold on
plot(lagsX,xcf1)
xlabel('time (s)')
ylabel('correlation')
title('cross-hemisphere beta correlation')

save('correlations','acf1','acf2','xcf1','lagsX')


%% make a figure after all of this stuff has loaded
cd '/home/sam/bucket/claustrum_sleep/Dropbox (OIST)'
folders={'2019.10.13';'2019.10.17';'2019.10.19';'2019.10.20';'2019.10.21'}
lags=0:1000
lagsX=-1000:1000

for f =1:numel(folders)
    cd([folders{f} filesep '_analysis'])
    load correlations.mat
    aCorr1Struct{f}=acf1;
    aCorr2Struct{f}=acf2;
    xCorrStruct{f}=xcf1;
    cd ..
    cd ..
end

figure
hold on
for f=1:numel(folders)
    
        plot(lags,aCorr1Struct{f}-f+1,'b')
        plot(lags,aCorr2Struct{f}-f+1,'r')
        if f==1
           plot(lags,aCorr1Struct{f}-f+1,'r') 
        end
        
    xlabel('time (s)')
    ylabel('correlation')
    legend('control','lesion')
    title('beta power autocorrelations')
end


figure
hold on
for f=1:numel(folders)
    
    if f==1
        plot(lagsX,xCorrStruct{f},'k','linewidth',2)
    elseif f==2
        plot(lagsX,xCorrStruct{f},'b')
    else
        plot(lagsX,xCorrStruct{f},'r')
    end
    
    xlabel('time (s)')
    ylabel('correlation')
    %legend('control','lesion')
    title('cross-hemisphere beta correlation')
end

%% get Sharp Waves. %channel and peak parameters adjusted for different datasets

sleepStart=3*60*60*1000; %start analysis 3 hours in. Animal is sleeping then.
sleepEnd=9*60*60*1000; %end analysis 9 hours in. Animal is still asleep.


[v,t]=SA.currentDataObj.getData(31,sleepStart,sleepEnd-sleepStart);
v=downsample(squeeze(v),32);
[bb,aa]=butter(2,[4/(1000/2)],'low');
vFilt=filtfilt(bb,aa,v);

prominanceThresh=1
heightThresh=1.5

[peaks,peakTime,widths,peakProminance]=findpeaks(-vFilt,...
    'MinPeakHeight',heightThresh*std(vFilt),'MinPeakDistance',200,'MinPeakProminence',prominanceThresh*std(vFilt), 'MinPeakWidth',5,'WidthReference','halfheight');


[v2,t]=SA.currentDataObj.getData(62,sleepStart,sleepEnd-sleepStart);
v2=downsample(squeeze(v2),32);
[bb,aa]=butter(2,[4/(1000/2)],'low');
vFilt2=filtfilt(bb,aa,v2);

[peaks2,peakTime2,widths,peakProminance]=findpeaks(-vFilt2,...
    'MinPeakHeight',heightThresh*std(vFilt2),'MinPeakDistance',200,'MinPeakProminence',prominanceThresh*std(vFilt2), 'MinPeakWidth',5,'WidthReference','halfheight');

%save('SWtimes','peakTime','peakTime2', 'prominanceThresh','heightThresh')
%% Look at sharp wave rate over slow cycles

cd '/home/sam/bucket/claustrum_sleep/Dropbox (OIST)'
folders={'2019.10.13';'2019.10.17';'2019.10.19';'2019.10.20';'2019.10.21'};
sleepStart=3*60*60*1000; %start analysis 3 hours in. Animal is sleeping then.
sleepEnd=9*60*60*1000; %end analysis 9 hours in. Animal is still asleep.


for f=1:numel(folders)
    
    cd([folders{f} filesep '_analysis']);
    load('slowCyclesBeta.mat');
    load('SWtimes.mat');
    peakTime=peakTime+sleepStart;
    peakTime2=peakTime2+sleepStart;
    
    %SWS as TcycleOffset:Onset
    swsStart=TcycleOffset(TcycleOffset>sleepStart & TcycleOffset<sleepEnd);
    %make the cycles nice in time, go from start to the nearest end
    swsEnd=[];
    for s=1:numel(swsStart)
        tmpEnd=TcycleOnset(TcycleOnset>swsStart(s));
        swsEnd(s)=tmpEnd(1);
    end
    %count SWs over cycles
    ch1SW=[];
    ch2SW=[];
    for c =1:numel(swsStart)
        ch1SW(c)=numel(find(peakTime>swsStart(c) & peakTime<swsEnd(c)))';
        ch2SW(c)=numel(find(peakTime2>swsStart(c) & peakTime2<swsEnd(c)))';
        
    end
    
    swStruct{f,1}=ch1SW;
    swStruct{f,2}=ch2SW;
    
    cd ..
    cd ..

    
end

%%% SW stats
biMat=cell2mat(swStruct(1,:)');
uniStruct=swStruct(2:end,:);
rateComp=cell2mat(uniStruct');

[~,order]=sort(rateComp(1,:));

figure
hold on
histogram(rateComp(1,:),'Normalization','probability')
histogram(rateComp(2,:),'Normalization','probability')
histogram([biMat(1,:) biMat(2,:)],'Normalization','probability')
legend('control','lesion','bilateral')
ylabel('probability')
xlabel('SW # within SWS cycle')


bigVec=[rateComp(1,:) rateComp(2,:) biMat(1,:) biMat(2,:)]
g=[ones(1,length(rateComp(1,:))) 2*ones(1,length(rateComp(2,:))) 3*ones(1,length(biMat(1,:))) 3*ones(1,length(biMat(2,:)))]
    
figure 
hold on
boxplot(bigVec,g,'Notch','on')
ylabel('SW # within SWS cycle')

[P,H,STATS]=signrank(rateComp(1,:),rateComp(2,:)) %Wilcoxon sign-rank test on the paired lesion/control, p<1e-59 (375 cycles, 2 animals, 4 nights)
p=signrank(biMat(1,:),biMat(2,:)) %Wilcoxon sign-rank test on the paired bilateral lesion/lesion, p=0.53 (108 cycles, 1 animal, 1 night)
p=ranksum(rateComp(1,:),biMat(1,:)) %Wilcoxon rank-sum for control and 1 hemisphere of bilaterally lesioned animals ...
%...(375 from 2 animals, 4 nights compared to 108 cycles, 1 animal, 1 night)


