
%% initial sleep analysis object
SA=sleepAnalysis;
SA=SA.getExcelData(SA.defaultXlsFile); %get recording information from excel sheet
SA=SA.setCurrentRecording(5);
SA=SA.getFilters;
%SA.par.DVRLFPCh{recordings(rec)}=64

SA.getSlowCycles('overwrite',1);
SA.getSharpWaves('detectOnlyDuringSWS',false,'overwrite',1);
SA.getSharpWavesAnalysis('overwrite',1);

%% for sleep recordings
 analysisStart=6*60*60*1000;
 analysisWin=2*60*60*1000;


%% get frequency bands
SA.getFreqBandDetection('tStart',analysisStart,'win',analysisWin);
SA.plotFreqBandDetection;

%% Delta to beta ratio calculation and plotting
SA.getDelta2BetaRatio; %to recalculate use: SA.getDelta2BetaRatio('overwrite',1);
SA.plotDelta2BetaRatio;

%% Calculate and plot the slow oscillation auto correlation and sliding auto correlation
%SA.getDelta2BetaAC('tStart',6*60*60*1000,'win',2*60*60*1000);

SA.getDelta2BetaAC('tStart',analysisStart,'win',analysisWin);
SA.plotDelta2BetaAC;
SA.plotDelta2BetaSlidingAC;

%% Extract and plot the onset and offsets of SWS/REM cycle
%SA.getSlowCycles;
%SA.plotSlowCycles;
SA.getSlowCyclesBeta;
%% Extract sharp waves
SA.getSharpWaves('detectOnlyDuringSWS',false);
SA.getSharpWavesAnalysis;
SA.plotSharpWavesAnalysis;

%%
I made plots from the 
