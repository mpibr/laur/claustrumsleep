%% invivo datasets

if ispc
    invivoDatasets ={'W:\chronicLizardExp\Claustrum\2019-05-09_18-23-09',...
        'W:\chronicLizardExp\Claustrum\2019-05-08_17-40-36',...
        'Z:\Lorenzo\claustrum in vivo\2019-05-26_17-59-49',...
        'Z:\Lorenzo\claustrum in vivo\2019-05-25_17-49-02',...
        'Z:\Lorenzo\claustrum in vivo\2019-05-24_17-51-43'}
else
    invivoDatasets ={'storage/laur/data_2/chronicLizardExp/Claustrum/2019-05-09_18-23-09',...
        '/storage/laur/data_2/chronicLizardExp/Claustrum/2019-05-08_17-40-36',...
        '/storage/laur/data_3/Lorenzo/claustrum in vivo/2019-05-26_17-59-49',...
        '/storage/laur/data_3/Lorenzo/claustrum in vivo/2019-05-25_17-49-02',...
        '/storage/laur/data_3/Lorenzo/claustrum in vivo/2019-05-24_17-51-43'}
end

%% Fig 1

% load data
dataObj=NLRecording (invivoDatasets{3});
[v,t]=dataObj.getData([32 64],2*60*1000*60,8*60*1000*60);  % load data for select channels and intervals

% filter and downsample data
[bb1,aa1]=butter(2,[150/(32000/2)],'low')
[bb2,aa2]=butter(2,[70/(32000/2) 150/(32000/2)])
[bb3,aa3]=butter(2,[250/(32000/2) 700/(32000/2)])

clear ('vDS1')
for ch=1:size(v,1)
    disp(ch)
    fV1=filtfilt(bb1,aa1,squeeze(v(ch,:)));
    fV2=filtfilt(bb2,aa2,squeeze(v(ch,:)));
    fV3=filtfilt(bb3,aa3,squeeze(v(ch,:)));
    vDS1(ch,:)=downsample(fV1,32);
    vDS2(ch,:)=downsample(fV2,32);
    vDS3(ch,:)=downsample(fV3,32);
end
%
figure
hold on
plot(((1850000:2400000)-1850000)/1000,vDS1(1,1850000:2400000)+350,'r')
plot(((1850000:2400000)-1850000)/1000,vDS1(2,1850000:2400000),'b')

plot(((1850000:2400000)-1850000)/1000,vDS2(1,1850000:2400000)+350,'k')
plot(((1850000:2400000)-1850000)/1000,vDS2(2,1850000:2400000),'k')

plot(((1850000:2400000)-1850000)/1000,vDS3(2,1850000:2400000)+100,'k')

xlabel('seconds')
ylabel('uV')
axis tight

xlim([0 449])
xlim([291 312])

%% delay between DVR and Claustrum: Xcorr
peakCorr=[]; allLags=[];xcf=[];
for dataset=1:5
    
    load([invivoDatasets{dataset} '_analysis/dbRatio_ch32.mat']);
    load([invivoDatasets{dataset} '_analysis/slowCycles_ch32.mat']);
    
    dbRatioInterp=interp1(t_ms,bufferedBetaRatio,(2*60*1000*60):(10*60*1000*60-1));
    scRatio=dbRatioInterp;
    
    [v,t]=dataObj.getData([32 64],2*60*1000*60,8*60*1000*60);
    
    [bb1,aa1]=butter(2,[150/(32000/2)],'low');
    
    clear ('vSWS','vSWS1')
    
    for ch=1:size(v,1)
        fV1=filtfilt(bb1,aa1,squeeze(v(ch,:)));
        fvDS1 = downsample(fV1,32);
        vSWS1(ch,:) = fvDS1(scRatio >= Th);
        vREM1(ch,:) = fvDS1(scRatio < Th);
    end
    
    %get cross-correlation
    
    [xcf(dataset,:),lags] = crosscorr(vSWS1(1,:),vSWS1(2,:),3000);
    [peakCorr(dataset) maxInd]=max(xcf(dataset,:));
    allLags(dataset)=lags(maxInd);
end
%
% figure
% plot(lags/1000,xcf,'linewidth',2)
% xlabel('seconds')
% ylabel('crosscorrelation')
% title('CL-DVR xcorr')
% axis tight
%
%%
% %crosscorr d/b
load([invivoDatasets{dataset} '_analysis/dbRatio_ch64.mat']);
dbRatioInterp2=interp1(t_ms,bufferedBetaRatio,(2*60*1000*60):(10*60*1000*60-1));

[xcf,lags] = crosscorr(dbRatioInterp,dbRatioInterp,1800000);
figure
plot(lags/1000,xcf,'linewidth',2)
xlabel('seconds')
ylabel('crosscorrelation')
title('CL-DVR xcorr')
axis tight


%%  delay between SWs in DVR and Claustrum
%SWRs detected by filter seperately in DVR and Cla, Taking closest pairs
%SWs if they occur within 500 ms of eachother.
dataset=3;

%load SWs
load([invivoDatasets{dataset} '_analysis/sharpWaves_ch64.mat']);
dvrSW=tSW;
load([invivoDatasets{dataset} '_analysis/sharpWaves_ch32.mat']);
claSW=tSW;

A = repmat(claSW,[1 length(dvrSW)]);
[minValue,closestIndex] = min(abs(A-dvrSW'));
timeDiffCla = claSW(closestIndex)-dvrSW;

A = repmat(dvrSW,[1 length(claSW)]);
[minValue,closestIndex] = min(abs(A-claSW'));
timeDiffDvr = dvrSW(closestIndex)-claSW;

timeDiffCla(abs(timeDiffCla)>500)=[];
timeDiffDvr(abs(timeDiffDvr)>500)=[];

figure
hold on
hist(timeDiffCla,-500:20:500)
hist(timeDiffDvr,-500:20:500)
axis tight
xlabel('msec')
ylabel('counts')
legend('CL -> DVR','DVR -> CL')
title('delays between SW peaks')

%% population analysis of the delays

% average over datasets of same animal
tCla=[];
tDvr=[];
for dataset=1:2
    load([invivoDatasets{dataset} '_analysis/sharpWaves_ch64.mat']);
    dvrSW=tSW;
    load([invivoDatasets{dataset} '_analysis/sharpWaves_ch32.mat']);
    claSW=tSW;
    
    A = repmat(claSW,[1 length(dvrSW)]);
    [minValue,closestIndex] = min(abs(A-dvrSW'));
    timeDiffCla = claSW(closestIndex)-dvrSW;
    
    A = repmat(dvrSW,[1 length(claSW)]);
    [minValue,closestIndex] = min(abs(A-claSW'));
    timeDiffDvr = dvrSW(closestIndex)-claSW;
    
    timeDiffCla(abs(timeDiffCla)>500)=[];
    timeDiffDvr(abs(timeDiffDvr)>500)=[];
    
    tCla{dataset}=timeDiffCla;
    tDvr{dataset}=timeDiffDvr;
end

claDelay=cell2mat(tCla');
DvrDelay=cell2mat(tDvr');

figure
hold on
histogram(claDelay,100)
histogram(DvrDelay,100)
axis tight
xlabel('msec')
ylabel('counts')
legend('CL -> DVR','DVR -> CL')
title(['delays between SW peaks, animal 1 averaged over 2 nights. N = ' ...
    num2str(numel(claDelay)) ' cla SWs, '  num2str(numel(DvrDelay)) ' dvr SWs, '])

figure
hold on
hist(timeDiffCla,-500:50:500)
hist(timeDiffDvr,-500:50:500)
axis tight
xlabel('msec')
ylabel('counts')
legend('CL -> DVR','DVR -> CL')
title(['delays between SW peaks, animal 1 averaged over 2 nights. N = ' ...
    num2str(numel(claDelay)) ' cla SWs, '  num2str(numel(DvrDelay)) ' dvr SWs, '])

tCla=[];
tDvr=[];
for dataset=3:5
    load([invivoDatasets{dataset} '_analysis/sharpWaves_ch64.mat']);
    dvrSW=tSW;
    load([invivoDatasets{dataset} '_analysis/sharpWaves_ch32.mat']);
    claSW=tSW;
    
    A = repmat(claSW,[1 length(dvrSW)]);
    [minValue,closestIndex] = min(abs(A-dvrSW'));
    timeDiffCla = claSW(closestIndex)-dvrSW;
    
    A = repmat(dvrSW,[1 length(claSW)]);
    [minValue,closestIndex] = min(abs(A-claSW'));
    timeDiffDvr = dvrSW(closestIndex)-claSW;
    
    timeDiffCla(abs(timeDiffCla)>500)=[];
    timeDiffDvr(abs(timeDiffDvr)>500)=[];
    
    tCla{dataset}=timeDiffCla;
    tDvr{dataset}=timeDiffDvr;
end

claDelay=cell2mat(tCla');
DvrDelay=cell2mat(tDvr');

figure
hold on
hist(claDelay,-500:20:500)
hist(DvrDelay,-500:20:500)
axis tight
xlabel('msec')
ylabel('counts')
legend('CL -> DVR','DVR -> CL')
title(['delays between SW peaks, animal 2 averaged over 3 nights. N = ' ...
    num2str(numel(claDelay)) ' cla SWs, '  num2str(numel(DvrDelay)) ' dvr SWs, '])

%% how do SW amplitude and rate depend on the DB ratio
dataset=3

%load SWs
load([invivoDatasets{dataset} '_analysis/sharpWaves_ch32.mat']);
load([invivoDatasets{dataset} '_analysis/slowCycles_ch32.mat']);
load([invivoDatasets{dataset} '_analysis/dbRatio_ch32.mat']);
load([invivoDatasets{dataset} '_analysis/sharpWavesAnalysis_ch32.mat']);
claSW=tSW;

swW=squeeze(allDS20Hz);
vSW=min(swW')';
badSW=abs(vSW)>500;
vSW(badSW)=[];
tSW(badSW)=[];


dataObj=NLRecording(invivoDatasets{dataset});
[v,t]=dataObj.getData([32],2*60*1000*60,1*60*1000*60);
fV=filtfilt(bb1,aa1,squeeze(v));
fvDS = downsample(fV,32);

figure
subplot 211
plot(fvDS)
hold on
plot(TcycleOffset-2*60*1000*60, 20*ones(1, numel(TcycleOffset)),'ro')
plot(tSW-2*60*1000*60, 15*ones(1, numel(tSW)),'ko')
xlim([TcycleOffset(36)-2*60*1000*60-40000 TcycleOffset(36)-2*60*1000*60+40000])

%DB ratio
dbInterp=interp1(t_ms, bufferedBetaRatio,1:t_ms(end));
[ev_avgDB,lagsDB,ev_mat] = getEventTrigAvg(dbInterp,TcycleOffset,80000,80000);

%SW rate
tSWtmp=tSW;
swBpp=zeros(1,tSWtmp(end));
swBpp(tSWtmp)=1;
[ev_avg,lags,ev_mat] = getEventTrigAvg(swBpp,TcycleOffset,80000,80000);

bins=1:100:size(ev_mat,2);

for bin=1:(numel(bins)-1)
    currBin=bins(bin);
    nextBin=bins(bin+1);
    currSWs=ev_mat(:,currBin:nextBin);
    if max(currSWs)==0
        meanAmp=0
    else
        currSWs(currSWs==0)=[];
        meanCount=sum(currSWs)/10;
    end
    swCountHist(bin)=meanCount;
    bin
end

swCounts=medfilt1(swCountHist,20); %in SWs/sec


%SW amp
tSWtmp=tSW;
swBpp=zeros(1,tSWtmp(end));
swBpp(tSWtmp)=-vSW;
[ev_avgAmp,lags,ev_matAmp] = getEventTrigAvg(swBpp,TcycleOffset,80000,80000);

bins=1:100:size(ev_matAmp,2);

for bin=1:(numel(bins)-1)
    currBin=bins(bin);
    nextBin=bins(bin+1);
    currSWs=ev_matAmp(:,currBin:nextBin);
    if max(currSWs)==0
        meanAmp=0
    else
        currSWs(currSWs==0)=[];
        meanAmp=mean(currSWs);
    end
    swAmpHist(bin)=meanAmp;
    bin
end

swAmps=medfilt1(swAmpHist,20); %in voltage

subplot 212
plot(lagsDB/1000,zscore(ev_avgDB))
hold on
%plot(lags/1000, smoothen(ev_avg', 2000,1000)*1000)
% plot(bins(1:end-1)/1000-80, (smoothen(swCounts, 25,1000)))
% plot(bins(1:end-1)/1000-80,(smoothen(swAmps,25,1000)))
% xlim([-40 40])
plot(bins(1:end-1)/1000-80, zscore(smoothen(swCounts, 25,1000)))
plot(bins(1:end-1)/1000-80,zscore(smoothen(swAmps,25,1000)))
xlim([-40 40])

legend('average db ratio','avg sw count','avg sw amp')
xlabel('time (s) centered on d->b transiont')
ylabel('amp (z)')

%% SWR characterization
dataset=3
%load SWs
load([invivoDatasets{dataset} '_analysis/sharpWaves_ch32.mat']);
load([invivoDatasets{dataset} '_analysis/slowCycles_ch32.mat']);
load([invivoDatasets{dataset} '_analysis/dbRatio_ch32.mat']);
load([invivoDatasets{dataset} '_analysis/sharpWavesAnalysis_ch32.mat']);

swW=squeeze(allDS20Hz);
vSW=min(swW')';
badSW=abs(vSW)>500;
vSW(badSW)=[];
tSW(badSW)=[];
swW(badSW,:)=[];

%restricting the SWs to during sleep
sleepInterp=interp1(t_ms, double(pSleepDBRatio'),1:t_ms(end));
sleepInterp(find(isnan(sleepInterp)))=0;
sleepSW=tSW;
tSW(~sleepInterp(sleepSW))=[];
vSW(~sleepInterp(sleepSW))=[];

avgWaveform=mean(swW);
halfmax=0.5*min(avgWaveform);

widths=zeros(1,size(swW,1));
peaks=zeros(1,size(swW,1));
for x=1:size(swW,1)
    [p,~,w,~] = findpeaks(-swW(x,:),  'WidthReference','halfheight');
    
    if numel(p)>1
        [peaks(x),ind]=max(p);
        widths(x)=w(ind);
    else
        peaks(x)=0;
        widths(x)=0;
    end
    x
end

badPeaks=find(peaks==0); % there is no width for these few peaks, definitely an artifact of detection
peaks(badPeaks)=[];
widths(badPeaks)=[];

widths=widths*mean(diff(tDS20Hz)); %to go up to ms, I am using the downsampled filtered output for speed up to now

figure
scatter(peaks,widths,5,'filled')
title('SW peaks and widths')
xlabel('amp (uV)')
ylabel('width (fwhm)')

figure
histogram(peaks,'Normalization','probability')
title('SW amplitude histogram')
xlabel('amp (uV)')
ylabel('probability')

figure
histogram(widths,'Normalization','probability')
title('SW width histogram')
xlabel('width (fwhm)')
ylabel('probability')


eb=std(swW);
figure
hold on
plot(tDS20Hz-1000, mean(swW),'k','linewidth',2)
plot(tDS20Hz-1000, mean(swW)+eb,'r')
plot(tDS20Hz-1000, mean(swW)-eb,'r')
title('avg invivo SW +/- 1 std')
ylabel('amp (uV)')
xlabel('time')
xlim([-1000 1500])

figure
histogram(diff(tSW1)/1000,'Normalization','probability')
title('SW inter-event interval distribution during sleep')
xlabel('time (s)')
ylabel('probability')


%% SWR characterization -- Population analyses

for dataset=1:5
    
    %load SWs
    load([invivoDatasets{dataset} '_analysis/sharpWaves_ch32.mat']);
    load([invivoDatasets{dataset} '_analysis/slowCycles_ch32.mat']);
    load([invivoDatasets{dataset} '_analysis/dbRatio_ch32.mat']);
    load([invivoDatasets{dataset} '_analysis/sharpWavesAnalysis_ch32.mat']);
    
    swW=squeeze(allDS20Hz);
    vSW=min(swW')';
    badSW=abs(vSW)>500;
    vSW(badSW)=[];
    tSW(badSW)=[];
    swW(badSW,:)=[];
    
    %restricting the SWs to during sleep
    sleepInterp=interp1(t_ms, double(pSleepDBRatio'),1:t_ms(end));
    sleepInterp(find(isnan(sleepInterp)))=0;
    sleepSW=tSW;
    avgWaveform=mean(swW);
    halfmax=0.5*min(avgWaveform);
    
    
    widths=zeros(1,size(swW,1));
    peaks=zeros(1,size(swW,1));
    for x=1:size(swW,1)
        [p,~,w,~] = findpeaks(-swW(x,:),  'WidthReference','halfheight');
        if numel(p)>0
            [peaks(x),ind]=max(p);
            widths(x)=w(ind);
        end
        x
    end
    peaks(peaks==0)=[];  %a few artifacts have no real height.
    widths(widths==0)=[];
    
    widths=widths*mean(diff(tDS20Hz)); %to go up to ms, I am using the downsampled filtered output for speed up to now
    
    meanWidth=mean(widths);
    meanAmp=mean(peaks);
    meanISI=mean(diff(tSW));
    nSWs=numel(peaks)
    save([invivoDatasets{dataset} '_analysis' filesep 'swProperties'],'meanWidth','meanISI','meanAmp','nSWs','peaks','widths')
end

